#!/usr/bin/env python
#
# vim: tabstop=4 shiftwidth=4

import traceback
import subprocess
import json

import base

from datetime import datetime

class CoreMediaLicense(base.Base):

    def __init__(self):
        base.Base.__init__(self)
        self.prefix = 'coremedia-license'

        self.LIC_VALID_FROM = 1
        self.LIC_UNTIL_HARD = 2
        self.LIC_UNTIL_SOFT = 3

        self.names = {
            1: 'from',
            2: 'hard',
            3: 'soft'
        }

    def get_stats(self):
        """
          Retrieves stats regarding latency to write to a test pool
        """
        data = {
            "Server": {
              "mbean": "com.coremedia:type=Server,application=%s" % ( self.application ),
              "attribute": [
                'LicenseValidFrom',
                'LicenseValidUntilHard',
                'LicenseValidUntilSoft',
                'ServiceInfos']
            }
        }

        result = self.read_jmx_data(data)

        jmx_data = self.parse_bean_data(result, "Server")

        # print(json.dumps( jmx_data, indent = 2 ))

        status   = jmx_data["status"]

        # mbean = "com.coremedia:type=Server,application={}".format( self.jmx_application )
        #
        # attribute_list = [
        #     'LicenseValidFrom',
        #     'LicenseValidUntilHard',
        #     'LicenseValidUntilSoft']
        #
        # status, jmx_data = self.read_jmx_data(mbean, attribute_list)

        data = {}

        if(status == 200):

            data[self.prefix] = {}
            data[self.prefix][self.application] = {}

            for lic_type in [self.LIC_UNTIL_SOFT, self.LIC_UNTIL_HARD]:

                difference, years, month, weeks, days, seconds = self.parse_license_date(jmx_data, lic_type)

                lic_type_named = self.names[lic_type]

                data[self.prefix][self.application][lic_type_named] = {}
                data[self.prefix][self.application][lic_type_named]['years'] = years
                data[self.prefix][self.application][lic_type_named]['month'] = month
                data[self.prefix][self.application][lic_type_named]['days'] = days

            self.logdebug( data )

        return data

    def parse_license_date( self, data, lic_type ):
        """

        """
        if( lic_type == self.LIC_UNTIL_HARD ):
          raw_date = data['LicenseValidUntilHard']
        elif( lic_type == self.LIC_UNTIL_SOFT ):
          raw_date = data['LicenseValidUntilSoft']
        else:
          raw_date = data['LicenseValidFrom']

        current_datetime = datetime.now()
        license_datetime = datetime.fromtimestamp( raw_date / 1000 )

        self.loginfo(
          "license type {type} = {license_datetime}".format(type=self.names[lic_type], license_datetime=license_datetime.strftime( "%d.%m.%Y" ) )
        )

        difference       = license_datetime - current_datetime
        duration_in_s    = difference.total_seconds()

        years            = divmod(duration_in_s, 31556952)[0]    # Seconds in a year = 365*24*60*60 = 31536000.
        month            = divmod(duration_in_s, 2628288)[0]
        weeks            = divmod(duration_in_s, 604800)[0]
        days             = divmod(duration_in_s, 86400)
        hours            = divmod(days[1], 3600)
        minutes          = divmod(hours[1], 60)

        return (license_datetime - current_datetime), years, month, weeks, days[0], duration_in_s


if __name__ == '__main__':

    sys.exit(0)
else:

    import collectd

    try:
        plugin = CoreMediaLicense()

    except Exception as exc:
        collectd.error("coremedia-license: failed to initialize plugin :: %s :: %s" % (exc, traceback.format_exc()))

    def configure_callback(conf):
        """Received configuration information"""
        plugin.config_callback(conf)

    def read_callback():
        """Callback triggerred by collectd on read"""
        plugin.read_callback()

    collectd.register_config(configure_callback)
    collectd.register_read(read_callback, plugin.interval)
