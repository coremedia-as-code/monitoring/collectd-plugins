#!/usr/bin/env python
# coding=utf-8
#
#  Copyright (c) 2019-2020 Bodo Schulz
#
#
# collectd-coremedia-license
#
# https://gitlab.cern.ch/ikadochn/collectd-eos/blob/master/src/collectd_eos/__init__.py
# https://gist.github.com/dizz/1250968

# from collections import defaultdict

import re
import collectd
import traceback

import json
from datetime import datetime
from pyjolokia import Jolokia, JolokiaError

# https://programtalk.com/vs2/?source=python/11188/collectd-ceph/plugins/base.py
#
class Base(object):

    def __init__(self):
        self.verbose = False
        self.debug = True
        self.prefix = ''
        self.interval = 60.0
        self.jolokia_url = "http://localhost:8080/jolokia/"
        self.application = None
        self.jmx_port = None
        self.jmx_application = None


    def config_callback(self, conf):
        """
          Takes a collectd conf object and fills in the local config.
        """
        for node in conf.children:
            if node.key == "Verbose":
                if node.values[0] in ['True', 'true']:
                    self.verbose = True
            elif node.key == "Debug":
                if node.values[0] in ['True', 'true']:
                    self.debug = True
            elif node.key == "Prefix":
                self.prefix = node.values[0]
            elif node.key == "Jolokia_Url":
                self.jolokia_url = node.values[0]
            elif node.key == "Application":
                self.application = node.values[0]
            elif node.key == "Jmx_Port":
                self.jmx_port = node.values[0]
            elif node.key == "Jmx_Application":
                self.jmx_application = node.values[0]
            elif node.key == 'Interval':
                self.interval = float(node.values[0])
            else:
                collectd.warning("%s: unknown config key: %s" % (self.prefix, node.key))


    def dispatch(self, stats):
        """
        Dispatches the given stats.

        stats should be something like:

        {'plugin': {'plugin_instance': {'type': {'type_instance': <value>, ...}}}}
        """
        if not stats:
            collectd.error("%s: failed to retrieve stats" % self.prefix)
            return

        self.logdebug("dispatching %d new stats :: %s" % (len(stats), stats))
        try:
            for plugin in stats.keys():
                for plugin_instance in stats[plugin].keys():
                    for type in stats[plugin][plugin_instance].keys():
                        type_value = stats[plugin][plugin_instance][type]
                        if not isinstance(type_value, dict):
                            self.dispatch_value(plugin, plugin_instance, type, None, type_value)
                        else:
                          for type_instance in stats[plugin][plugin_instance][type].keys():
                              self.dispatch_value(plugin, plugin_instance,
                                      type, type_instance,
                                      stats[plugin][plugin_instance][type][type_instance])

        except Exception as exc:
            collectd.error("%s: failed to dispatch values :: %s :: %s" % (self.prefix, exc, traceback.format_exc()))


    def dispatch_value(self, plugin, plugin_instance, type, type_instance, value):
        """
          Looks for the given stat in stats, and dispatches it
        """
        self.logdebug("dispatching value %s.%s.%s.%s = %s" % (plugin, plugin_instance, type, type_instance, value))

        val = collectd.Values(type='gauge')
        val.plugin = plugin

        if type_instance is not None:
            # val.type_instance="%s-%s" % (type, type_instance)
            plugin_instance = "%s.%s.%s" % ( plugin_instance, type, type_instance )
#        else:
#            val.type_instance=type

        val.plugin_instance = plugin_instance

        val.values=[value]
        val.interval = self.interval

        self.logdebug("sent metric %s.%s.%s.%s.%s" % (plugin, plugin_instance, type, type_instance, value))
        self.logdebug('metric to dispatch is: ' + str(val))

        val.dispatch()


    def read_callback(self):
        try:
            start = datetime.now()
            stats = self.get_stats()
            self.logverbose("collectd new data from service :: took %d seconds" % (datetime.now() - start).seconds)
        except Exception as exc:
            collectd.error("%s: failed to get stats :: %s :: %s" % (self.prefix, exc, traceback.format_exc()))

        self.dispatch(stats)


    def get_stats(self):
        collectd.error('Not implemented, should be subclassed')


    def parse_bean_data(self, json_data, bean):
        """
          parse mbean data and return only one request
        """
        d = {}
        for v in json_data:
            status = v["status"]
            if(status == 200):
                d = v['value']

                if(re.findall(r".*{}$".format(bean), d['mbean'])):
                    break
                else:
                    d = {}
            else:
                d = { "status": status }

        return d


    def read_jmx_data(self, jolokia_data):
        """
          jolokia_data :

          data = {
              "Server": {
                "mbean": 'com.coremedia:type=Server,application=replication-live-server',
                "attribute": [
                  'RepositorySequenceNumber',
                  'RunLevelNumeric',
                  'RunLevel']
              },
              "Replicator": {
                "mbean": 'com.coremedia:type=Replicator,application=replication-live-server',
                "attribute": [
                  'ConnectionUp',
                  'ControllerState',
                  'LatestCompletedStampedNumber',
                  'MasterLiveServerIORUrl']
              }
          }
        """

        jolokia_url    = self.jolokia_url
        jolokia_target = "service:jmx:rmi:///jndi/rmi://localhost:%d/jmxrmi" % (int(self.jmx_port))
        # jolokia_url    = "http://%s:8080/jolokia/" % (jolokia_host)
        # jolokia_target = "service:jmx:rmi:///jndi/rmi://localhost:{port!r}/jmxrmi".format( port = int(self.jmx_port) )

        print(jolokia_url)
        print(jolokia_target)

        j4p = Jolokia( jolokia_url )
        # j4p.auth(httpusername='jolokia', httppassword='jolokia')
        j4p.config( ignoreErrors = 'true', ifModifiedSince = 'true', canonicalNaming = 'true' )
        j4p.target( url = jolokia_target )

        for instance in jolokia_data.keys():

            if isinstance(jolokia_data[instance], dict):
                mbean          = jolokia_data[instance]["mbean"]
                attribute_list = jolokia_data[instance]["attribute"]

                # print(mbean)
                # print(attribute_list)

                j4p.add_request(
                    type = 'read',
                    mbean = mbean,
                    attribute =  (",".join(attribute_list)) )


        response = j4p.getRequests()

        # rebuild dict
        for v in response:

            status = v["status"]

            if( status == 200 ):
                data = v['value']
                data["timestamp"] = v["timestamp"]
            else:
                v["value"] = {}
                data = v['value']

            data["status"] = status
            data["mbean"] = v["request"]["mbean"]

            try:
                v.pop("request")
                v.pop("stacktrace")
                v.pop("error")
            except KeyError:
                continue

        return response

    def loginfo(self, msg):
        collectd.info("%s: %s" % (self.prefix, msg))

    def logverbose(self, msg):
        if self.verbose:
            collectd.verbose("%s: %s" % (self.prefix, msg))

    def logdebug(self, msg):
        if self.debug:
            collectd.debug("%s: %s" % (self.prefix, msg))
