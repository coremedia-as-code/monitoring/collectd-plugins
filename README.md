# collectd plugins for CoreMedia Monitoring


## CoreMedia Licenses
```
davros.coremedia-license-CMS-soft-years.gauge -1 1578497549
davros.coremedia-license-CMS-soft-month.gauge -1 1578497549
davros.coremedia-license-CMS-soft-days.gauge -9 1578497489
davros.coremedia-license-CMS-hard-years.gauge 0 1578497489
davros.coremedia-license-CMS-hard-month.gauge 0 1578497489
davros.coremedia-license-CMS-hard-days.gauge 6 1578497489
```

```
cat /etc/collectd/license.conf

LoadPlugin python

# ...
<Plugin python>
  ModulePath "/opt/src/coremedia-as-code/monitoring/collectd-plugins"
  # LogTraces true
  # Interactive true
  Import "coremedia_license"

  <Module coremedia_license>
    # Debug true
    # Verbose true
    Interval 240
    Prefix "CMS"
    Application "content-management-server"
    Jmx_Port 40199
    Jolokia_Url "http://backend.cm.local:8080/jolokia"
  </Module>
</Plugin>

```
