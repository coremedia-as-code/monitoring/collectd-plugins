#!/usr/bin/env python
# coding=utf-8
#
#  Copyright (c) 2019-2020 Bodo Schulz
#
#
# collectd-coremedia-license
#
# https://gitlab.cern.ch/ikadochn/collectd-eos/blob/master/src/collectd_eos/__init__.py
# https://gist.github.com/dizz/1250968

# from collections import defaultdict

import collectd
# import os
# import socket
# import subprocess
import json
from datetime import datetime
from pyjolokia import Jolokia, JolokiaError

# # python 3 moved urlparse
# try:
#     from urlparse import urlparse
# except ImportError:
#     from urllib.parse import urlparse

class CM_License(object):

    def __init__(self, interval = 120, application, jmx_port ):
        self.plugin_name = 'collectd-coremedia-license'
        self.interval = interval
        self.application = application
        self.jmx_port = jmx_port


    def config(self, obj):
        for node in obj.children:
            if node.key == 'Port':
                self.mongo_port = int(node.values[0])
            elif node.key == 'Host':
                self.mongo_host = node.values[0]
            elif node.key == 'User':
                self.mongo_user = node.values[0]
            elif node.key == 'Password':
                self.mongo_password = node.values[0]
            elif node.key == 'Database':
                self.mongo_db = node.values
            else:
                self.log("Unknown configuration key %s" % node.key)


    def log_verbose(self, msg):
        if not self.verbose_logging:
            return
        collectd.info('%s plugin [verbose]: %s' % (self.plugin_name, msg))


# def restore_sigchld():
#     """
#     Restore SIGCHLD handler for python <= v2.6
#     It will BREAK exec plugin!!!
#     See https://github.com/deniszh/collectd-iostat-python/issues/2 for details
#     """
#     if sys.version_info[0] == 2 and sys.version_info[1] <= 6:
#         signal.signal(signal.SIGCHLD, signal.SIG_DFL)


if __name__ == '__main__':
    lic = CM_License()
    ds = lic.get_data()

    for disk in ds:
        for metric in ds[disk]:
            tbl = string.maketrans('/-%', '___')
            metric_name = metric.translate(tbl)
            print("%s.%s:%s" % (disk, metric_name, ds[disk][metric]))

    sys.exit(0)
else:
    import collectd

    lic = CM_License()

    # Register callbacks
    collectd.register_config(lic.config)
    # collectd.register_init(restore_sigchld)
    collectd.register_config(lic.configure_callback)




PLUGIN_NAME = "license"

CONFIG_DEFAULT_JMX_PORT = None        #
CONFIG_DEFAULT_JMX_APPLICATION = None #
CONFIG_DEFAULT_INTERVAL = -1          # use collectd default

LIC_VALID_FROM = 1
LIC_UNTIL_HARD = 2
LIC_UNTIL_SOFT = 3

DEBUG = True

def _debug(*args, **kwargs):
    if DEBUG:
        collectd.debug( *args, **kwargs )

def _parse_config_node(conf):
    for node in conf.children:
        if node.children:
            collectd.warning( "Plugin {plugin}: unexpected config block {block!r}, ignoring".format( plugin = PLUGIN_NAME, block = node.key ) )
            continue
        yield node.key.lower(), node.values


def _parse_config_keys(key_value_list):
    instances = []
    intervals = []
    for key, values in key_value_list:
        if key == "interval":
            intervals.append(values)
        elif key == "instance":
            instances.append(values)
        else:
            collectd.warning(
              "Plugin {plugin}: unexpected config key: {key!r}, ignoring"
              .format(plugin=PLUGIN_NAME, key=key))

    return instances, intervals


def _check_instances(instances):
    _debug("Plugin {plugin}: configuring monitored instances".format(plugin=PLUGIN_NAME))

    url_count = defaultdict(int)
    name_count = defaultdict(int)
    for name, url in instances:
        name_count[name] += 1
        url_count[url] += 1
        if name_count[name] > 1:
            raise collectd.CollectdError(
              "Plugin {plugin}: plugin instance names must be unique, found reused: {name!r}"
              .format(plugin=PLUGIN_NAME, name=name))
        if url_count[url] > 1:
            collectd.warning(
              "Plugin {plugin}: configuring instance {instance!r} mgm_url {url!r}, "
              "URL already used in another instance"
              .format(plugin=PLUGIN_NAME, instance=name, url=url))
    return instances


def _check_instance(values):
    if len(values) != 2:
        raise collectd.CollectdError(
          "Plugin {plugin}: instance expects 2 values: application_name, jmxport; "
          "found values: {values!r}"
          .format(plugin=PLUGIN_NAME, values=values))

    return values


def _parse_interval(intervals):

    _debug("Plugin {plugin}: configuring interval".format(plugin=PLUGIN_NAME))

    if len(intervals) == 0:
        _debug("Plugin {plugin}: using default interval".format(plugin=PLUGIN_NAME))
        return CONFIG_DEFAULT_INTERVAL
    elif len(intervals) == 1:
        _debug("Plugin {plugin}: configuring custom interval".format(plugin=PLUGIN_NAME))
        val = intervals[0]
        if len(val) != 1:
            raise collectd.CollectdError(
              "Plugin {plugin}: interval key expects 1 value, found: {n}"
              .format(plugin=PLUGIN_NAME, n=val))
        _debug(
          "Plugin {plugin}: configured custom interval: {interval!r}"
          .format(plugin=PLUGIN_NAME, interval=intervals[0][0]))
        return intervals[0][0]
    else:
        raise collectd.CollectdError(
            "Plugin {plugin}: found {n} interval keys in config, expecting no more than one"
            .format(plugin=PLUGIN_NAME, n=len(intervals)))


def _read_jmx_data(application, jmx_port):

    jolokia_url = "http://backend.cm.local:8080/jolokia/"
    jolokia_target = "service:jmx:rmi:///jndi/rmi://localhost:{port!r}/jmxrmi".format( port = jmx_port)
    jolokia_mbean = "com.coremedia:type=Server,application={}".format( application )

    status = 404
    data   = {}
    response = {}

    j4p = Jolokia( jolokia_url )
    j4p.config( ignoreErrors = 'true', ifModifiedSince = 'true', canonicalNaming = 'true' )
    j4p.target( url = jolokia_target )

    try:
        response = j4p.request(
          type = 'read',
          mbean = jolokia_mbean,
          attribute = 'LicenseValidFrom,LicenseValidUntilHard,LicenseValidUntilSoft'
        )

#        print(type(response))
#        print(type(data))

    except Exception as e:
        raise collectd.CollectdError(
            JolokiaError('JolokiaError: Could not connect. Got error %s' % (e) ) )

    status = response["status"]

    if( status == 200 ):
      try:
        response.pop("request")
      except KeyError:
        print("Key not found")

      data = response['value']
      data["timestamp"] = response["timestamp"]
    else:
      data = {}

    data["status"] = status

    return status, data


def _parse_license_date( data, type = LIC_UNTIL_HARD ):

  if( type == LIC_UNTIL_HARD ):
    raw_date = data['LicenseValidUntilHard']
  elif( type == LIC_UNTIL_SOFT ):
    raw_date = data['LicenseValidUntilSoft']
  else:
    raw_date = data['LicenseValidFrom']

  # print(raw_date)

  current_datetime = datetime.now()
  license_datetime = datetime.fromtimestamp( raw_date / 1000 )

  collectd.info(
    "Plugin {plugin}: license type {type} = {license_datetime}".format(plugin=PLUGIN_NAME, type=type, license_datetime=license_datetime.strftime( "%d.%m.%Y" ) )
  )

  difference       = license_datetime - current_datetime
  duration_in_s    = difference.total_seconds()

  years            = divmod(duration_in_s, 31536000)[0]    # Seconds in a year = 365*24*60*60 = 31536000.
  month            = divmod(duration_in_s, 2628288)[0]
  weeks            = divmod(duration_in_s, 604800)[0]
  days             = divmod(duration_in_s, 86400)
  hours            = divmod(days[1], 3600)
  minutes          = divmod(hours[1], 60)

  return (license_datetime - current_datetime), years, month, weeks, days[0], duration_in_s



def read_callback(data):

    name, port, interval = data

    _debug(
      "Plugin {plugin}: application {application!r}: read callback with port {port!r}, interval {interval!r} "
      .format(plugin=PLUGIN_NAME, application=name, port=port, interval=interval) )

    status, data = _read_jmx_data( name, int(port) )

    #_debug(status)
    #_debug(data)

    # carbon-writer.$host.MLS.Server.license.until.soft.days
    # carbon-writer.$host.CMS.Server.license.until.soft.weeks
    # carbon-writer.$host.CMS.Server.license.until.soft.month
    # carbon-writer.$host.CMS.Server.license.until.hard.month
    # carbon-writer.$host.CMS.Server.ServiceInfo.feeder.concurrent.diff
    # carbon-writer.$host.CMS.Server.ServiceInfo.feeder.named.diff
    # MLS / CMS:
    # carbon-writer.$host.CMS.Server.ServiceInfo.publisher.concurrent.diff
    # carbon-writer.$host.CMS.Server.ServiceInfo.publisher.named.diff
    # RLS
    # carbon-writer.$host.RLS.Server.ServiceInfo.webserver.concurrent.diff
    #
    # carbon-writer.$host.CMS.Server.ServiceInfo.replicator.concurrent.diff
    #
    # carbon-writer.$host.CMS.Server.ServiceInfo.system.concurrent.diff
    #
    if(status == 200):

        plugin_name = "coremedia-license"

        metric = collectd.Values()
        metric.type = 'gauge'
        metric.plugin = "coremedia-license"

        # TAG support (planned)
        # metric.plugin = "%s;application=%s,type=%s,until_in=%s" %( plugin_name, "content-management-server", "soft", "days" )

        for lic_type in [LIC_UNTIL_SOFT, LIC_UNTIL_HARD]:

            difference, years, month, weeks, days, seconds = _parse_license_date(data, lic_type)

            lic_type_named = "soft"
            if(lic_type == LIC_UNTIL_HARD):
                lic_type_named = "hard"

            # dispatch data ...
            #
            # days
            # $hostname.coremedia-license-until_soft_days.gauge -9 1578487757
            metric.plugin_instance = "until_{}_days".format(lic_type_named)
            metric.values=[days]
            metric.meta = { "application": "content-management-server", "type": lic_type_named, "until_in": "days" }
            metric.interval = interval

            collectd.info('metric to dispatch is: ' + str(metric))

            metric.dispatch()

            # weeks
            # $hostname.coremedia-license-until_soft_weeks.gauge -2 1578487847
            #
            metric.plugin_instance = "until_{}_weeks".format(lic_type_named)
            metric.values=[weeks]
            metric.meta = { "application": "content-management-server", "type": lic_type_named, "until_in": "weeks" }
            metric.interval = interval

            collectd.info('metric to dispatch is: ' + str(metric))

            metric.dispatch()

            # years
            # $hostname.coremedia-license-until_soft_years.gauge -1 1578487857
            #
            metric.plugin_instance = "until_{}_years".format(lic_type_named)
            # metric.type_instance = "until.soft.years"
            metric.values=[years]
            metric.meta = { "application": "content-management-server", "type": lic_type_named, "until_in": "years" }
            metric.interval = interval

            collectd.info('metric to dispatch is: ' + str(metric))

            metric.dispatch()


def configure_callback(conf):

    config = _parse_config_node(conf)
    instance_vals, interval_vals = _parse_config_keys(config)

    instances = _check_instances( [_check_instance(vals) for vals in instance_vals] )

    if not instances:
        raise collectd.CollectdError( "Plugin {plugin}: needs an instance".format(plugin=PLUGIN_NAME) )

    interval = _parse_interval(interval_vals)

    for name, port in instances:
        collectd.info(
          "Plugin {plugin}: configured application {application!r} with jmx port={port!r}, "
          "interval={interval!r}"
          .format(plugin=PLUGIN_NAME, application=name, port=int(port), interval=interval) )

        kwargs = dict(callback=read_callback, data=(name, int(port), interval), name=name)

        if interval > 0:
            kwargs["interval"] = interval

        collectd.register_read(**kwargs)


collectd.register_config(configure_callback)

